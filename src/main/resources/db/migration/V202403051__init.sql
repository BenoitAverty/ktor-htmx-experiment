create table connections (
    tid UUID not null primary key,
    name varchar(255) not null unique,
    url varchar(255) not null unique,
    username varchar(255) not null,
    password varchar(255) not null
);
