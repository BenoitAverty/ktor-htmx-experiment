package com.baverty

import com.baverty.configuration.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.cors.routing.*

fun main(args: Array<String>) {
    EngineMain.main(args)
}

fun Application.module() {
// Removes for the demo
//    install(FlywayPlugin.plugin)
    install(CORS) {
        anyHost()
        allowNonSimpleContentTypes = true
        allowMethod(HttpMethod.Options)
        allowMethod(HttpMethod.Put)
        allowMethod(HttpMethod.Patch)
        allowMethod(HttpMethod.Delete)
        allowMethod(HttpMethod.Post)
        allowHeadersPrefixed("HX-")
    }
    configureKoin()
    configureThymeleaf()

    serveStaticResources()
    configureTemplating()
}
