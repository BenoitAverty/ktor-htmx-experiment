package com.baverty.infrastructure

import com.baverty.domain.DumpService
import com.baverty.domain.model.Connection
import com.baverty.domain.model.Dump
import com.baverty.domain.model.Progress
import java.time.Instant
import java.util.*

class FakeDumpService: DumpService {

    private val dumps = mutableMapOf<UUID, Dump>()

    override suspend fun initializeOrGetDump(connection: Connection): Dump {
        val dump = dumps.computeIfAbsent(connection.tid) { FakeDump(Progress(0), "/dumps/${connection.tid}-${Instant.now().epochSecond}.gz") }
        dump.progress += (1..10).random()
        return dump
    }

}

class FakeDump(
    override var progress: Progress,
    override val downloadUrl: String
) : Dump
