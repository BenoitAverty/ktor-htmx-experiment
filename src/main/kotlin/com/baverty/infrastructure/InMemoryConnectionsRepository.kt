package com.baverty.infrastructure

import com.baverty.domain.ConnectionsRepository
import com.baverty.domain.model.Connection
import java.util.UUID

class InMemoryConnectionsRepository: ConnectionsRepository {
    private val connections = mutableListOf<Connection>(
        Connection(UUID.fromString("2c0e8b21-6d13-42ec-a9f7-bee0c6ded6b2"), "Local db", "jdbc:postgres://localhost:5432/postgres", "root", "mysecretpassword")
    )

    override suspend fun findAll(): List<Connection> {
        return connections
    }

    override suspend fun save(connection: Connection) {
        delete(connection.tid)
        connections.add(connection)
    }

    override suspend fun delete(tid: UUID) {
        connections.removeIf { it.tid == tid }
    }

    override suspend fun findByTid(tid: UUID): Connection? {
        return connections.find { it.tid == tid }
    }
}
