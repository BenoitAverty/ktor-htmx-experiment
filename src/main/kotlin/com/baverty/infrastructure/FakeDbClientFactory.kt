package com.baverty.infrastructure

import com.baverty.domain.DbClient
import com.baverty.domain.DbClientFactory
import com.baverty.domain.model.*
import kotlinx.coroutines.delay

class FakeDbClientFactory: DbClientFactory {
    private val tables = mutableListOf(
        Table("customers", listOf(
            Column("tid", "UUID", "PRIMARY KEY"),
            Column("customer_number", "INTEGER", "NOT NULL"),
            Column("first_name", "TEXT", "NOT NULL"),
            Column("last_name", "TEXT", "NOT NULL"),
            Column("telephone_number", "varchar(20)", ""),
            Column("city", "", "NOT NULL"),
        )),
        Table("orders", listOf(
            Column("tid", "UUID", "PRIMARY KEY"),
            Column("customer_number_tid", "UUID", "NOT NULL REFERENCES customers(tid)"),
            Column("total_amount", "DECIMAL", "NOT NULL"),
            Column("status", "VARCHAR(10)", "NOT NULL"),
        )),
        Table("catalog", listOf(
            Column("tid", "UUID", "PRIMARY KEY"),
            Column("item_name", "TEXT", "NOT NULL"),
            Column("reference", "INTEGER", "NOT NULL"),
            Column("quantity_in_stock", "INTEGER", "NOT NULL"),
        ))
    )

    override fun getDbClient(connection: Connection): DbClient {
        return FakeDbClient(tables)
    }
}


private class FakeDbClient(private val tables: MutableList<Table>): DbClient {
    override suspend fun listTables(): List<Table> {
        delay(5000)
        return tables
    }

    override suspend fun executeQuery(sql: String): QueryResult {
        if(sql.lowercase().startsWith("select * from catalog")) {
            return ResultSet(listOf(
                mapOf("tid" to "5c91fac0-94fc-444a-8deb-4e8ced809fb1", "item_name" to "Lightbulbs", "reference" to "123456", "quantity_in_stock" to 37),
                mapOf("tid" to "74a5fdcf-727d-41bc-a742-82d0597f3da5", "item_name" to "One Ring", "reference" to "3791", "quantity_in_stock" to 1),
                mapOf("tid" to "6576909f-d094-4423-af17-fbf7bbe0aaa4", "item_name" to "Starter Pokemon", "reference" to "99999", "quantity_in_stock" to 3),
                mapOf("tid" to "e527f89f-15ac-4158-99eb-ee0617d32f59", "item_name" to "Fullstack developer", "reference" to "1764648", "quantity_in_stock" to 0),
            ), listOf("tid", "item_name", "reference", "quantity_in_stock"))
        }
        else if(sql.lowercase().replace("\\s".toRegex(), " ").startsWith("create table greetings (name ")) {
            tables.add(0,
                Table(
                    "greetings",
                    listOf(Column("name", "text", "default 'World' not null"))
                )
            )

            return DDLQuerySuccess()
        }
        else if(sql.lowercase().startsWith("insert into greetings")) {
            return UpdateQuerySuccess(1)
        }
        else {
            return QueryFailure("I'm only a demo, I can't do that. Sorry.")
        }
    }
}
