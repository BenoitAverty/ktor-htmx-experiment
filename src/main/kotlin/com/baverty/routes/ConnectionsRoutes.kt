package com.baverty.routes

import com.baverty.domain.ConnectionsRepository
import com.baverty.domain.model.Connection
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.thymeleaf.*
import org.koin.ktor.ext.inject
import java.util.*

fun Routing.connectionsRouting() {
    val connectionsRepository by inject<ConnectionsRepository>()

    get("/connections") {
        val connections = connectionsRepository.findAll()

        call.respondTemplate("views/connections", mapOf("connections" to connections))
    }

    post("/connections") {
        val parameters = call.receiveParameters()

        val newConnection = Connection(
            UUID.randomUUID(),
            parameters["conn_name"].toString(),
            parameters["conn_url"].toString(),
            parameters["conn_username"].toString(),
            parameters["conn_password"].toString()
        )

        connectionsRepository.save(newConnection)

        call.respondTemplate("fragments/connections_table", mapOf("connection" to newConnection))
    }

    delete("/connections/{tid}") {
        connectionsRepository.delete(UUID.fromString(call.parameters["tid"]!!))

        // Unfortunately htmx will only swap on 200, not 204 (without some more code)
        // See https://github.com/bigskysoftware/htmx/issues/1130
        call.respond(HttpStatusCode.OK)
    }
}
