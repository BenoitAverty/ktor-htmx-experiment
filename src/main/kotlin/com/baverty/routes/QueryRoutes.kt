package com.baverty.routes

import com.baverty.domain.ConnectionsRepository
import com.baverty.domain.DbClientFactory
import com.baverty.domain.DumpService
import com.baverty.domain.model.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.plugins.cachingheaders.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.thymeleaf.*
import jdk.incubator.vector.VectorOperators.Test
import org.koin.ktor.ext.inject
import java.util.*

fun Routing.queryRouting() {
    val connectionsRepository by inject<ConnectionsRepository>()
    val dbClientFactory by inject<DbClientFactory>()
    val dumpService by inject<DumpService>()

    get("/connections/{tid}/query") {
        val connection = connectionsRepository.findByTid(UUID.fromString(call.parameters["tid"]!!))

        if(connection == null) {
            call.respondTemplate("views/not_found")
        }
        else {
            call.respondTemplate("views/query", mapOf(
                "connection" to connection,
            ))
        }
    }

    get("/connections/{tid}/query/tables") {
        call.caching = CachingOptions(CacheControl.MaxAge(60, visibility = CacheControl.Visibility.Private))

        val connection = connectionsRepository.findByTid(UUID.fromString(call.parameters["tid"]!!))

        val dbClient = dbClientFactory.getDbClient(connection!!) // TODO handle 404

        call.respondTemplate("fragments/query_tool_tables", mapOf(
            "tables" to dbClient.listTables(),
            "connection" to connection
        ))

    }

    post("/connections/{tid}/query") {
        val connection = connectionsRepository.findByTid(UUID.fromString(call.parameters["tid"]!!))

        val dbClient = dbClientFactory.getDbClient(connection!!) // TODO handle 404

        val parameters = call.receiveParameters()
        val queryResult = dbClient.executeQuery(parameters["query_sql"] ?: "")

        when(queryResult) {
            is ResultSet -> call.respondTemplate("fragments/result_set", mapOf(
                "columns" to queryResult.columns,
                "data" to queryResult
            ))
            is DDLQuerySuccess -> {
                call.response.headers.append("HX-Trigger", "business:schema-updated")
                call.respondTemplate("fragments/message", mapOf("status" to "success", "message" to "OK"))
            }
            is QueryFailure ->
                call.respondTemplate("fragments/message", mapOf("status" to "error", "message" to queryResult.errorMessage))
            is UpdateQuerySuccess ->
                call.respondTemplate("fragments/message", mapOf("status" to "success", "message" to "${queryResult.affectedRows} rows affected."))
        }
    }

    post("/connections/{tid}/dump") {
        val connection = connectionsRepository.findByTid(UUID.fromString(call.parameters["tid"]!!))

        val dump = dumpService.initializeOrGetDump(connection!!) // TODO handle 404

        call.respondTemplate("fragments/dump_progress", mapOf("progress" to dump.progress.percentage, "url" to dump.downloadUrl, "connection" to connection))
    }
}
