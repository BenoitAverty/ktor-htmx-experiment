package com.baverty.domain.model

class Table(
    val name: String,
    val columns: List<Column>,
) {
}
