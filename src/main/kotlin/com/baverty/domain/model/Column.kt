package com.baverty.domain.model

class Column(
    val name: String,
    val type: String, // Should be an enum
    val attributes: String, // Should be a list of better type
)
