package com.baverty.domain.model

import java.util.UUID

class Connection(
    val tid: UUID,
    val name: String,
    val url: String,
    val username: String,
    val password: String,
) {
    fun canBeConnectedTo(): Boolean = url.isNotBlank()
}
