package com.baverty.domain.model

interface Dump {
    var progress: Progress
    val downloadUrl: String
}

data class Progress(val percentage: Int) {
    init {
        if(percentage > 100 || percentage < 0) throw IllegalArgumentException("Percentage is between 0 and 100")
    }

    operator fun plus(increment: Int) = Progress((this.percentage + increment).coerceAtMost(100))
}
