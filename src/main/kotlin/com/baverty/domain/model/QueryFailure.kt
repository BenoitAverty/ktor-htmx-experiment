package com.baverty.domain.model

class QueryFailure(val errorMessage: String) : QueryResult
