package com.baverty.domain.model

class ResultSet(private val data: List<Map<String, Any>>, val columns: List<String>) : QueryResult, List<Map<String, Any>> by data {
}
