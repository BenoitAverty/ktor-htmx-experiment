package com.baverty.domain

import com.baverty.domain.model.Connection
import java.util.*

interface ConnectionsRepository {
    suspend fun findAll(): List<Connection>
    suspend fun save(connection: Connection)
    suspend fun delete(tid: UUID)
    suspend fun findByTid(fromString: UUID): Connection?
}
