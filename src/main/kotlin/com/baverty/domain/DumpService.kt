package com.baverty.domain

import com.baverty.domain.model.Connection
import com.baverty.domain.model.Dump

interface DumpService {
    /**
     * If there is a database dump in progress for the given connection, retrieve its status. Otherwise, start a dump and return it.
     */
    suspend fun initializeOrGetDump(connection: Connection): Dump
}
