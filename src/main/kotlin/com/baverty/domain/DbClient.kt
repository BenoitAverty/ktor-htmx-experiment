package com.baverty.domain

import com.baverty.domain.model.QueryResult
import com.baverty.domain.model.Table

interface DbClient {
    suspend fun listTables(): List<Table>
    suspend fun executeQuery(sql: String): QueryResult
}
