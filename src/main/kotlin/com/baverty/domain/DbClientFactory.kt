package com.baverty.domain

import com.baverty.domain.model.Connection

interface DbClientFactory {
    fun getDbClient(connection: Connection): DbClient
}
