package com.baverty.configuration

import io.ktor.server.application.*
import io.ktor.server.application.hooks.*
import org.flywaydb.core.Flyway

private val logger = io.github.oshai.kotlinlogging.KotlinLogging.logger {}

// Removed for demo
object FlywayPlugin {
    val plugin = createApplicationPlugin("flyway") {
        on(MonitoringEvent(ApplicationStarted)) { application ->
            try {
                val flyway: Flyway = Flyway.configure()
                    .driver("postgresql")
                    .dataSource("jdbc:postgresql://localhost:5432/postgres", "postgres", "mysecretpassword")
                    .load()

                flyway.migrate()
            } catch (e: Exception) {
                logger.error(e) { "Could not migrate database" }
            }
        }
    }
}
