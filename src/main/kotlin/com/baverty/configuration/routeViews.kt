package com.baverty.configuration

import com.baverty.routes.connectionsRouting
import com.baverty.routes.queryRouting
import io.ktor.server.application.*
import io.ktor.server.plugins.cachingheaders.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureTemplating() {

    install(IgnoreTrailingSlash)
    install(CachingHeaders)

    routing {
        get("/") {
            call.respondRedirect("/connections")
        }

        connectionsRouting()
        queryRouting()
    }
}
