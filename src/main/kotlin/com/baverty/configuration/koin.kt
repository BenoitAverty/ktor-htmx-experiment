package com.baverty.configuration

import com.baverty.domain.ConnectionsRepository
import com.baverty.domain.DbClientFactory
import com.baverty.domain.DumpService
import com.baverty.infrastructure.FakeDbClientFactory
import com.baverty.infrastructure.FakeDumpService
import com.baverty.infrastructure.InMemoryConnectionsRepository
import io.ktor.server.application.*
import org.koin.dsl.module
import org.koin.ktor.plugin.Koin

private val appModule = module {
    single<ConnectionsRepository> { InMemoryConnectionsRepository() }
    single<DbClientFactory> { FakeDbClientFactory() }
    single<DumpService> { FakeDumpService() }
}

fun Application.configureKoin() {
    install(Koin) {
        modules(appModule)
    }
}
